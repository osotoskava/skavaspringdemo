package com.skava.SkavaSpringDemo.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

import com.skava.SkavaSpringDemo.entities.Partner;

public interface PartnerRepository extends Repository<Partner, Long> {
	
	Partner findByName(String name);

}
