package com.skava.SkavaSpringDemo.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.skava.SkavaSpringDemo.entities.Partner;

@Component("partnerService")
public class PartnerServiceImpl implements PartnerService {
	
	private final PartnerRepository partnerRepository;

	public PartnerServiceImpl(PartnerRepository partnerRepository) {
		this.partnerRepository = partnerRepository;
	}

	@Override
	public Partner findByName(String name) {
		return this.partnerRepository.findByName(name);
	}
	

}
