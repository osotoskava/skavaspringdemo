package com.skava.SkavaSpringDemo.services;

import org.springframework.util.Assert;

public class PartnerSearchCriteria {
	
	private String name;

	public PartnerSearchCriteria() {
	}

	public PartnerSearchCriteria(String name) {
//		Assert.notNull(name, "Name must not be null");
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
