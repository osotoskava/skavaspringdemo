package com.skava.SkavaSpringDemo.services;

import com.skava.SkavaSpringDemo.entities.Partner;

public interface PartnerService {
	
	Partner findByName(String name);

}
