package com.skava.SkavaSpringDemo;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.skava.util.ZkPropertiesFactoryBean;
import com.skava.util.ZookeeperHelper;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.skava.SkavaSpringDemo.services")
public class AppJPAConfig {
	
	@Autowired
	private ZkPropertiesFactoryBean zkProperties;

//	@Resource
//	private Environment env;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		ZookeeperHelper zkHelper = zkProperties.getZookeeperHelper();
		
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl(zkHelper.get("mcdburl.master"));
		dataSource.setUsername(zkHelper.get("mcdb.user"));
		dataSource.setPassword(zkHelper.get("mcdb.userpwd"));

		return dataSource;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(dataSource());
//		em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
		em.setPackagesToScan("com.skava.SkavaSpringDemo.entities");
		
		final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(hibProperties());

		return em;
	}

	private Properties hibProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect",	"org.hibernate.dialect.MySQL5InnoDBDialect");
		properties.put("hibernate.show_sql", "true");
//		properties.put("hibernate.hbm2ddl.auto", "validate");
		return properties;
	}
	
	@Bean
	public JpaTransactionManager transactionManager() {
		final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
	}

}
