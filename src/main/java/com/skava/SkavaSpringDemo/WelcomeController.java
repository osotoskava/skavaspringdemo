package com.skava.SkavaSpringDemo;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.skava.SkavaSpringDemo.entities.Partner;
import com.skava.SkavaSpringDemo.services.PartnerService;
import com.skava.util.ZkPropertiesFactoryBean;

@Controller
public class WelcomeController {

	// inject via application.properties
	@Value("${welcome.message:test}")
	private String message = "Hello World";
	
	// message from zookeeper
//	@Value("${message}")
//	private String zkMessage;
	
	@Autowired
	private ZkPropertiesFactoryBean zkProperties;
	
	@Autowired
	private PartnerService partnerService;

	@RequestMapping("/")
	public String welcome(Map<String, Object> model,
			@RequestParam(value = "message", required = false) String message) {
		if(message==null || "".equals(message))
			message = this.message;
		model.put("message", message);
		return "welcome";
	}
	
	@RequestMapping("/zk")
	public String welcomeZKMessage(Map<String, Object> model) {
		String message = zkProperties.getZookeeperHelper().get("message");
		model.put("message", message);
		return "welcome";
	}
	
	/**
	 * CRUD on Partner table
	 * @param model
	 * @return
	 */
	@RequestMapping("/partner")
	public String getPartners(Map<String, Object> model) {
		Partner partner = partnerService.findByName("macys");
		model.put("partner", partner);
		return "partner";
	}
	
}