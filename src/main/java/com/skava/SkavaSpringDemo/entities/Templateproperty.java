package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Templateproperty generated by hbm2java
 */
@Entity
@Table(name = "templateproperty", catalog = "mcdb")
public class Templateproperty implements java.io.Serializable {

	private TemplatepropertyId id;
	private Template template;
	private int type;
	private String defaultvalue;

	public Templateproperty() {
	}

	public Templateproperty(TemplatepropertyId id, Template template, int type) {
		this.id = id;
		this.template = template;
		this.type = type;
	}

	public Templateproperty(TemplatepropertyId id, Template template, int type, String defaultvalue) {
		this.id = id;
		this.template = template;
		this.type = type;
		this.defaultvalue = defaultvalue;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "templateid", column = @Column(name = "templateid", nullable = false)),
			@AttributeOverride(name = "name", column = @Column(name = "name", nullable = false, length = 100)) })
	public TemplatepropertyId getId() {
		return this.id;
	}

	public void setId(TemplatepropertyId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "templateid", nullable = false, insertable = false, updatable = false)
	public Template getTemplate() {
		return this.template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	@Column(name = "type", nullable = false)
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Column(name = "defaultvalue", length = 250)
	public String getDefaultvalue() {
		return this.defaultvalue;
	}

	public void setDefaultvalue(String defaultvalue) {
		this.defaultvalue = defaultvalue;
	}

}
