package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * ProductupcmapperId generated by hbm2java
 */
@Embeddable
public class ProductupcmapperId implements java.io.Serializable {

	private String upc;
	private String appname;

	public ProductupcmapperId() {
	}

	public ProductupcmapperId(String upc, String appname) {
		this.upc = upc;
		this.appname = appname;
	}

	@Column(name = "upc", nullable = false, length = 50)
	public String getUpc() {
		return this.upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	@Column(name = "appname", nullable = false, length = 20)
	public String getAppname() {
		return this.appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof ProductupcmapperId))
			return false;
		ProductupcmapperId castOther = (ProductupcmapperId) other;

		return ((this.getUpc() == castOther.getUpc())
				|| (this.getUpc() != null && castOther.getUpc() != null && this.getUpc().equals(castOther.getUpc())))
				&& ((this.getAppname() == castOther.getAppname()) || (this.getAppname() != null
						&& castOther.getAppname() != null && this.getAppname().equals(castOther.getAppname())));
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + (getUpc() == null ? 0 : this.getUpc().hashCode());
		result = 37 * result + (getAppname() == null ? 0 : this.getAppname().hashCode());
		return result;
	}

}
