package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Bundle generated by hbm2java
 */
@Entity
@Table(name = "bundle", catalog = "mcdb")
public class Bundle implements java.io.Serializable {

	private Long id;
	private Campaign campaign;
	private long flags;
	private String name;
	private String description;
	private String defaults;
	private String value;
	private Set<Campaigntemplate> campaigntemplates = new HashSet<Campaigntemplate>(0);

	public Bundle() {
	}

	public Bundle(Campaign campaign, long flags, String name, String description, String defaults, String value) {
		this.campaign = campaign;
		this.flags = flags;
		this.name = name;
		this.description = description;
		this.defaults = defaults;
		this.value = value;
	}

	public Bundle(Campaign campaign, long flags, String name, String description, String defaults, String value,
			Set<Campaigntemplate> campaigntemplates) {
		this.campaign = campaign;
		this.flags = flags;
		this.name = name;
		this.description = description;
		this.defaults = defaults;
		this.value = value;
		this.campaigntemplates = campaigntemplates;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "campaignid", nullable = false)
	public Campaign getCampaign() {
		return this.campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@Column(name = "flags", nullable = false)
	public long getFlags() {
		return this.flags;
	}

	public void setFlags(long flags) {
		this.flags = flags;
	}

	@Column(name = "name", nullable = false, length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description", nullable = false, length = 200)
	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "defaults", nullable = false, length = 200)
	public String getDefaults() {
		return this.defaults;
	}

	public void setDefaults(String defaults) {
		this.defaults = defaults;
	}

	@Column(name = "value", nullable = false, length = 16777215)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bundle")
	public Set<Campaigntemplate> getCampaigntemplates() {
		return this.campaigntemplates;
	}

	public void setCampaigntemplates(Set<Campaigntemplate> campaigntemplates) {
		this.campaigntemplates = campaigntemplates;
	}

}
