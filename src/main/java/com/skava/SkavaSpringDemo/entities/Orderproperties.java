package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Orderproperties generated by hbm2java
 */
@Entity
@Table(name = "orderproperties", catalog = "mcdb")
public class Orderproperties implements java.io.Serializable {

	private Long id;
	private Orders orders;
	private String name;
	private String value;
	private Date createdtime;
	private Date lastupdatedtime;

	public Orderproperties() {
	}

	public Orderproperties(String name, Date createdtime, Date lastupdatedtime) {
		this.name = name;
		this.createdtime = createdtime;
		this.lastupdatedtime = lastupdatedtime;
	}

	public Orderproperties(Orders orders, String name, String value, Date createdtime, Date lastupdatedtime) {
		this.orders = orders;
		this.name = name;
		this.value = value;
		this.createdtime = createdtime;
		this.lastupdatedtime = lastupdatedtime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "orderid")
	public Orders getOrders() {
		return this.orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "value", length = 16777215)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdtime", nullable = false, length = 19)
	public Date getCreatedtime() {
		return this.createdtime;
	}

	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastupdatedtime", nullable = false, length = 19)
	public Date getLastupdatedtime() {
		return this.lastupdatedtime;
	}

	public void setLastupdatedtime(Date lastupdatedtime) {
		this.lastupdatedtime = lastupdatedtime;
	}

}
