package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Upcproperties generated by hbm2java
 */
@Entity
@Table(name = "upcproperties", catalog = "mcdb")
public class Upcproperties implements java.io.Serializable {

	private UpcpropertiesId id;
	private Partner partner;
	private String prop1;
	private String prop2;
	private String prop3;
	private String prop4;
	private String prop5;
	private String avail;
	private String skuid;
	private Integer onhand;
	private String statsentryid;

	public Upcproperties() {
	}

	public Upcproperties(UpcpropertiesId id, Partner partner) {
		this.id = id;
		this.partner = partner;
	}

	public Upcproperties(UpcpropertiesId id, Partner partner, String prop1, String prop2, String prop3, String prop4,
			String prop5, String avail, String skuid, Integer onhand, String statsentryid) {
		this.id = id;
		this.partner = partner;
		this.prop1 = prop1;
		this.prop2 = prop2;
		this.prop3 = prop3;
		this.prop4 = prop4;
		this.prop5 = prop5;
		this.avail = avail;
		this.skuid = skuid;
		this.onhand = onhand;
		this.statsentryid = statsentryid;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "partnerid", column = @Column(name = "partnerid", nullable = false)),
			@AttributeOverride(name = "productid", column = @Column(name = "productid", nullable = false, length = 100)),
			@AttributeOverride(name = "upcid", column = @Column(name = "upcid", nullable = false, length = 30)) })
	public UpcpropertiesId getId() {
		return this.id;
	}

	public void setId(UpcpropertiesId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partnerid", nullable = false, insertable = false, updatable = false)
	public Partner getPartner() {
		return this.partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	@Column(name = "prop1")
	public String getProp1() {
		return this.prop1;
	}

	public void setProp1(String prop1) {
		this.prop1 = prop1;
	}

	@Column(name = "prop2", length = 1000)
	public String getProp2() {
		return this.prop2;
	}

	public void setProp2(String prop2) {
		this.prop2 = prop2;
	}

	@Column(name = "prop3")
	public String getProp3() {
		return this.prop3;
	}

	public void setProp3(String prop3) {
		this.prop3 = prop3;
	}

	@Column(name = "prop4")
	public String getProp4() {
		return this.prop4;
	}

	public void setProp4(String prop4) {
		this.prop4 = prop4;
	}

	@Column(name = "prop5")
	public String getProp5() {
		return this.prop5;
	}

	public void setProp5(String prop5) {
		this.prop5 = prop5;
	}

	@Column(name = "avail", length = 10)
	public String getAvail() {
		return this.avail;
	}

	public void setAvail(String avail) {
		this.avail = avail;
	}

	@Column(name = "skuid", length = 30)
	public String getSkuid() {
		return this.skuid;
	}

	public void setSkuid(String skuid) {
		this.skuid = skuid;
	}

	@Column(name = "onhand")
	public Integer getOnhand() {
		return this.onhand;
	}

	public void setOnhand(Integer onhand) {
		this.onhand = onhand;
	}

	@Column(name = "statsentryid")
	public String getStatsentryid() {
		return this.statsentryid;
	}

	public void setStatsentryid(String statsentryid) {
		this.statsentryid = statsentryid;
	}

}
