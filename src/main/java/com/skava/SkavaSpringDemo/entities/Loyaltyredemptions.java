package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Loyaltyredemptions generated by hbm2java
 */
@Entity
@Table(name = "loyaltyredemptions", catalog = "mcdb")
public class Loyaltyredemptions implements java.io.Serializable {

	private Long recordid;
	private Partner partner;
	private Usersv2 usersv2;
	private String xactid;
	private String redemptiontype;
	private String redemptionkey;
	private String redemptionstatus;
	private Integer redemptionvalue;
	private String title;
	private String imageurl;
	private String itemurl;
	private Date datetimeawarded;
	private Date expireson;

	public Loyaltyredemptions() {
	}

	public Loyaltyredemptions(Partner partner, Usersv2 usersv2, String xactid, Date datetimeawarded, Date expireson) {
		this.partner = partner;
		this.usersv2 = usersv2;
		this.xactid = xactid;
		this.datetimeawarded = datetimeawarded;
		this.expireson = expireson;
	}

	public Loyaltyredemptions(Partner partner, Usersv2 usersv2, String xactid, String redemptiontype,
			String redemptionkey, String redemptionstatus, Integer redemptionvalue, String title, String imageurl,
			String itemurl, Date datetimeawarded, Date expireson) {
		this.partner = partner;
		this.usersv2 = usersv2;
		this.xactid = xactid;
		this.redemptiontype = redemptiontype;
		this.redemptionkey = redemptionkey;
		this.redemptionstatus = redemptionstatus;
		this.redemptionvalue = redemptionvalue;
		this.title = title;
		this.imageurl = imageurl;
		this.itemurl = itemurl;
		this.datetimeawarded = datetimeawarded;
		this.expireson = expireson;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "recordid", unique = true, nullable = false)
	public Long getRecordid() {
		return this.recordid;
	}

	public void setRecordid(Long recordid) {
		this.recordid = recordid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "partnerid", nullable = false)
	public Partner getPartner() {
		return this.partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid", nullable = false)
	public Usersv2 getUsersv2() {
		return this.usersv2;
	}

	public void setUsersv2(Usersv2 usersv2) {
		this.usersv2 = usersv2;
	}

	@Column(name = "xactid", nullable = false, length = 100)
	public String getXactid() {
		return this.xactid;
	}

	public void setXactid(String xactid) {
		this.xactid = xactid;
	}

	@Column(name = "redemptiontype", length = 50)
	public String getRedemptiontype() {
		return this.redemptiontype;
	}

	public void setRedemptiontype(String redemptiontype) {
		this.redemptiontype = redemptiontype;
	}

	@Column(name = "redemptionkey", length = 50)
	public String getRedemptionkey() {
		return this.redemptionkey;
	}

	public void setRedemptionkey(String redemptionkey) {
		this.redemptionkey = redemptionkey;
	}

	@Column(name = "redemptionstatus", length = 65535)
	public String getRedemptionstatus() {
		return this.redemptionstatus;
	}

	public void setRedemptionstatus(String redemptionstatus) {
		this.redemptionstatus = redemptionstatus;
	}

	@Column(name = "redemptionvalue")
	public Integer getRedemptionvalue() {
		return this.redemptionvalue;
	}

	public void setRedemptionvalue(Integer redemptionvalue) {
		this.redemptionvalue = redemptionvalue;
	}

	@Column(name = "title", length = 65535)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "imageurl", length = 65535)
	public String getImageurl() {
		return this.imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	@Column(name = "itemurl", length = 65535)
	public String getItemurl() {
		return this.itemurl;
	}

	public void setItemurl(String itemurl) {
		this.itemurl = itemurl;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "datetimeawarded", nullable = false, length = 19)
	public Date getDatetimeawarded() {
		return this.datetimeawarded;
	}

	public void setDatetimeawarded(Date datetimeawarded) {
		this.datetimeawarded = datetimeawarded;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expireson", nullable = false, length = 19)
	public Date getExpireson() {
		return this.expireson;
	}

	public void setExpireson(Date expireson) {
		this.expireson = expireson;
	}

}
