package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Storemaster generated by hbm2java
 */
@Entity
@Table(name = "storemaster", catalog = "mcdb")
public class Storemaster implements java.io.Serializable {

	private StoremasterId id;
	private int status;
	private String name;
	private String type;
	private String city;
	private String state;
	private String country;
	private String zip;
	private String latitude;
	private String longitude;
	private Date starttime;
	private Date endtime;
	private Date createdtime;
	private long createdby;
	private Date updatedtime;
	private long updatedby;

	public Storemaster() {
	}

	public Storemaster(StoremasterId id, int status, String name, String type, Date starttime, Date endtime,
			Date createdtime, long createdby, Date updatedtime, long updatedby) {
		this.id = id;
		this.status = status;
		this.name = name;
		this.type = type;
		this.starttime = starttime;
		this.endtime = endtime;
		this.createdtime = createdtime;
		this.createdby = createdby;
		this.updatedtime = updatedtime;
		this.updatedby = updatedby;
	}

	public Storemaster(StoremasterId id, int status, String name, String type, String city, String state,
			String country, String zip, String latitude, String longitude, Date starttime, Date endtime,
			Date createdtime, long createdby, Date updatedtime, long updatedby) {
		this.id = id;
		this.status = status;
		this.name = name;
		this.type = type;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
		this.latitude = latitude;
		this.longitude = longitude;
		this.starttime = starttime;
		this.endtime = endtime;
		this.createdtime = createdtime;
		this.createdby = createdby;
		this.updatedtime = updatedtime;
		this.updatedby = updatedby;
	}

	@EmbeddedId

	@AttributeOverrides({ @AttributeOverride(name = "id", column = @Column(name = "id", nullable = false)),
			@AttributeOverride(name = "campaignid", column = @Column(name = "campaignid", nullable = false)),
			@AttributeOverride(name = "storeid", column = @Column(name = "storeid", nullable = false, length = 50)) })
	public StoremasterId getId() {
		return this.id;
	}

	public void setId(StoremasterId id) {
		this.id = id;
	}

	@Column(name = "status", nullable = false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "name", nullable = false, length = 250)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", nullable = false, length = 20)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "city", length = 250)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state", length = 250)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "country", length = 250)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "zip", length = 250)
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "latitude", length = 20)
	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "longitude", length = 20)
	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "starttime", nullable = false, length = 19)
	public Date getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endtime", nullable = false, length = 19)
	public Date getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdtime", nullable = false, length = 19)
	public Date getCreatedtime() {
		return this.createdtime;
	}

	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	@Column(name = "createdby", nullable = false)
	public long getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedtime", nullable = false, length = 19)
	public Date getUpdatedtime() {
		return this.updatedtime;
	}

	public void setUpdatedtime(Date updatedtime) {
		this.updatedtime = updatedtime;
	}

	@Column(name = "updatedby", nullable = false)
	public long getUpdatedby() {
		return this.updatedby;
	}

	public void setUpdatedby(long updatedby) {
		this.updatedby = updatedby;
	}

}
