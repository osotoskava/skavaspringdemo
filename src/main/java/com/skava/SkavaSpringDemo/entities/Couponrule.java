package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Couponrule generated by hbm2java
 */
@Entity
@Table(name = "couponrule", catalog = "mcdb")
public class Couponrule implements java.io.Serializable {

	private Integer id;
	private int type;
	private String name;
	private String value;

	public Couponrule() {
	}

	public Couponrule(int type, String name, String value) {
		this.type = type;
		this.name = name;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "type", nullable = false)
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Column(name = "name", nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "value", nullable = false, length = 8000)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
