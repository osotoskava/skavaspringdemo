package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Useridentityv2 generated by hbm2java
 */
@Entity
@Table(name = "useridentityv2", catalog = "mcdb")
public class Useridentityv2 implements java.io.Serializable {

	private Long id;
	private Usersv2 usersv2;
	private int type;
	private String value;
	private String userid;
	private String channel;
	private Date lastloggedintime;
	private Date createdtime;
	private Date updatedtime;

	public Useridentityv2() {
	}

	public Useridentityv2(Usersv2 usersv2, int type, String value, String userid, Date lastloggedintime,
			Date createdtime, Date updatedtime) {
		this.usersv2 = usersv2;
		this.type = type;
		this.value = value;
		this.userid = userid;
		this.lastloggedintime = lastloggedintime;
		this.createdtime = createdtime;
		this.updatedtime = updatedtime;
	}

	public Useridentityv2(Usersv2 usersv2, int type, String value, String userid, String channel, Date lastloggedintime,
			Date createdtime, Date updatedtime) {
		this.usersv2 = usersv2;
		this.type = type;
		this.value = value;
		this.userid = userid;
		this.channel = channel;
		this.lastloggedintime = lastloggedintime;
		this.createdtime = createdtime;
		this.updatedtime = updatedtime;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "skavauserid", nullable = false)
	public Usersv2 getUsersv2() {
		return this.usersv2;
	}

	public void setUsersv2(Usersv2 usersv2) {
		this.usersv2 = usersv2;
	}

	@Column(name = "type", nullable = false)
	public int getType() {
		return this.type;
	}

	public void setType(int type) {
		this.type = type;
	}

	@Column(name = "value", nullable = false, length = 100)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Column(name = "userid", nullable = false, length = 100)
	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Column(name = "channel", length = 100)
	public String getChannel() {
		return this.channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastloggedintime", nullable = false, length = 19)
	public Date getLastloggedintime() {
		return this.lastloggedintime;
	}

	public void setLastloggedintime(Date lastloggedintime) {
		this.lastloggedintime = lastloggedintime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdtime", nullable = false, length = 19)
	public Date getCreatedtime() {
		return this.createdtime;
	}

	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedtime", nullable = false, length = 19)
	public Date getUpdatedtime() {
		return this.updatedtime;
	}

	public void setUpdatedtime(Date updatedtime) {
		this.updatedtime = updatedtime;
	}

}
