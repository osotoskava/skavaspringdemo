package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Pluginversions generated by hbm2java
 */
@Entity
@Table(name = "pluginversions", catalog = "mcdb")
public class Pluginversions implements java.io.Serializable {

	private Integer id;
	private String version;
	private Plugin plugin;
	private String releasenotes;
	private Date releasedate;
	private String details;

	public Pluginversions() {
	}

	public Pluginversions(Plugin plugin, String releasenotes, Date releasedate, String details) {
		this.plugin = plugin;
		this.releasenotes = releasenotes;
		this.releasedate = releasedate;
		this.details = details;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

//	@Version
	@Column(name = "version", nullable = false, length = 10)
	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pluginid", nullable = false)
	public Plugin getPlugin() {
		return this.plugin;
	}

	public void setPlugin(Plugin plugin) {
		this.plugin = plugin;
	}

	@Column(name = "releasenotes", nullable = false, length = 1000)
	public String getReleasenotes() {
		return this.releasenotes;
	}

	public void setReleasenotes(String releasenotes) {
		this.releasenotes = releasenotes;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "releasedate", nullable = false, length = 19)
	public Date getReleasedate() {
		return this.releasedate;
	}

	public void setReleasedate(Date releasedate) {
		this.releasedate = releasedate;
	}

	@Column(name = "details", nullable = false, length = 10000)
	public String getDetails() {
		return this.details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
