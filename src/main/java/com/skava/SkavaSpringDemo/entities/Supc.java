package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Supc generated by hbm2java
 */
@Entity
@Table(name = "supc", catalog = "mcdb")
public class Supc implements java.io.Serializable {

	private Long id;
	private Campaign campaign;
	private Users usersByTargetuserid;
	private Users usersByUserid;
	private String machineid;
	private String type;
	private String param1;
	private String groupcode;
	private String promocode;

	public Supc() {
	}

	public Supc(Campaign campaign, String promocode) {
		this.campaign = campaign;
		this.promocode = promocode;
	}

	public Supc(Campaign campaign, Users usersByTargetuserid, Users usersByUserid, String machineid, String type,
			String param1, String groupcode, String promocode) {
		this.campaign = campaign;
		this.usersByTargetuserid = usersByTargetuserid;
		this.usersByUserid = usersByUserid;
		this.machineid = machineid;
		this.type = type;
		this.param1 = param1;
		this.groupcode = groupcode;
		this.promocode = promocode;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "campaignid", nullable = false)
	public Campaign getCampaign() {
		return this.campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "targetuserid")
	public Users getUsersByTargetuserid() {
		return this.usersByTargetuserid;
	}

	public void setUsersByTargetuserid(Users usersByTargetuserid) {
		this.usersByTargetuserid = usersByTargetuserid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid")
	public Users getUsersByUserid() {
		return this.usersByUserid;
	}

	public void setUsersByUserid(Users usersByUserid) {
		this.usersByUserid = usersByUserid;
	}

	@Column(name = "machineid", length = 20)
	public String getMachineid() {
		return this.machineid;
	}

	public void setMachineid(String machineid) {
		this.machineid = machineid;
	}

	@Column(name = "type")
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "param1")
	public String getParam1() {
		return this.param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	@Column(name = "groupcode")
	public String getGroupcode() {
		return this.groupcode;
	}

	public void setGroupcode(String groupcode) {
		this.groupcode = groupcode;
	}

	@Column(name = "promocode", nullable = false, length = 30)
	public String getPromocode() {
		return this.promocode;
	}

	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}

}
