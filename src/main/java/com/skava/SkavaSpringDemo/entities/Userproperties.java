package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Userproperties generated by hbm2java
 */
@Entity
@Table(name = "userproperties", catalog = "mcdb")
public class Userproperties implements java.io.Serializable {

	private Long id;
	private Users users;
	private String name;
	private String value;

	public Userproperties() {
	}

	public Userproperties(Users users, String name) {
		this.users = users;
		this.name = name;
	}

	public Userproperties(Users users, String name, String value) {
		this.users = users;
		this.name = name;
		this.value = value;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "userid", nullable = false)
	public Users getUsers() {
		return this.users;
	}

	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name = "name", nullable = false, length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "value", length = 65535)
	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
