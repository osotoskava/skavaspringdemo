package com.skava.SkavaSpringDemo.entities;
// Generated May 31, 2017 2:22:52 PM by Hibernate Tools 5.2.3.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Productmaster generated by hbm2java
 */
@Entity
@Table(name = "productmaster", catalog = "mcdb", uniqueConstraints = @UniqueConstraint(columnNames = { "campaignid",
		"productid" }))
public class Productmaster implements java.io.Serializable {

	private Long id;
	private Campaign campaign;
	private String productid;
	private int status;
	private String name;
	private int iscollection;
	private int islocked;
	private String subproductid1;
	private String subproductid2;
	private String subproductid3;
	private String subproductid4;
	private String subproductid5;
	private String subproductid6;
	private String subproductid7;
	private String subproductid8;
	private String subproductid9;
	private String subproductid10;
	private String subproductid11;
	private String subproductid12;
	private String subproductid13;
	private String subproductid14;
	private String subproductid15;
	private String upsellproductid1;
	private String upsellproductid2;
	private String upsellproductid3;
	private String upsellproductid4;
	private String upsellproductid5;
	private String upsellproductid6;
	private String upsellproductid7;
	private String upsellproductid8;
	private String upsellproductid9;
	private String upsellproductid10;
	private String crosssellproductid1;
	private String crosssellproductid2;
	private String crosssellproductid3;
	private String crosssellproductid4;
	private String crosssellproductid5;
	private String crosssellproductid6;
	private String crosssellproductid7;
	private String crosssellproductid8;
	private String crosssellproductid9;
	private String crosssellproductid10;
	private String defaultparentcategoryid;
	private Date starttime;
	private Date endtime;
	private Date createdtime;
	private long createdby;
	private Date updatedtime;
	private long updatedby;
	private Set<Productmasterproperties> productmasterpropertieses = new HashSet<Productmasterproperties>(0);
	private Set<Reviewsmaster> reviewsmasters = new HashSet<Reviewsmaster>(0);

	public Productmaster() {
	}

	public Productmaster(Campaign campaign, String productid, int status, String name, int iscollection, int islocked,
			Date starttime, Date endtime, Date createdtime, long createdby, Date updatedtime, long updatedby) {
		this.campaign = campaign;
		this.productid = productid;
		this.status = status;
		this.name = name;
		this.iscollection = iscollection;
		this.islocked = islocked;
		this.starttime = starttime;
		this.endtime = endtime;
		this.createdtime = createdtime;
		this.createdby = createdby;
		this.updatedtime = updatedtime;
		this.updatedby = updatedby;
	}

	public Productmaster(Campaign campaign, String productid, int status, String name, int iscollection, int islocked,
			String subproductid1, String subproductid2, String subproductid3, String subproductid4,
			String subproductid5, String subproductid6, String subproductid7, String subproductid8,
			String subproductid9, String subproductid10, String subproductid11, String subproductid12,
			String subproductid13, String subproductid14, String subproductid15, String upsellproductid1,
			String upsellproductid2, String upsellproductid3, String upsellproductid4, String upsellproductid5,
			String upsellproductid6, String upsellproductid7, String upsellproductid8, String upsellproductid9,
			String upsellproductid10, String crosssellproductid1, String crosssellproductid2,
			String crosssellproductid3, String crosssellproductid4, String crosssellproductid5,
			String crosssellproductid6, String crosssellproductid7, String crosssellproductid8,
			String crosssellproductid9, String crosssellproductid10, String defaultparentcategoryid, Date starttime,
			Date endtime, Date createdtime, long createdby, Date updatedtime, long updatedby,
			Set<Productmasterproperties> productmasterpropertieses, Set<Reviewsmaster> reviewsmasters) {
		this.campaign = campaign;
		this.productid = productid;
		this.status = status;
		this.name = name;
		this.iscollection = iscollection;
		this.islocked = islocked;
		this.subproductid1 = subproductid1;
		this.subproductid2 = subproductid2;
		this.subproductid3 = subproductid3;
		this.subproductid4 = subproductid4;
		this.subproductid5 = subproductid5;
		this.subproductid6 = subproductid6;
		this.subproductid7 = subproductid7;
		this.subproductid8 = subproductid8;
		this.subproductid9 = subproductid9;
		this.subproductid10 = subproductid10;
		this.subproductid11 = subproductid11;
		this.subproductid12 = subproductid12;
		this.subproductid13 = subproductid13;
		this.subproductid14 = subproductid14;
		this.subproductid15 = subproductid15;
		this.upsellproductid1 = upsellproductid1;
		this.upsellproductid2 = upsellproductid2;
		this.upsellproductid3 = upsellproductid3;
		this.upsellproductid4 = upsellproductid4;
		this.upsellproductid5 = upsellproductid5;
		this.upsellproductid6 = upsellproductid6;
		this.upsellproductid7 = upsellproductid7;
		this.upsellproductid8 = upsellproductid8;
		this.upsellproductid9 = upsellproductid9;
		this.upsellproductid10 = upsellproductid10;
		this.crosssellproductid1 = crosssellproductid1;
		this.crosssellproductid2 = crosssellproductid2;
		this.crosssellproductid3 = crosssellproductid3;
		this.crosssellproductid4 = crosssellproductid4;
		this.crosssellproductid5 = crosssellproductid5;
		this.crosssellproductid6 = crosssellproductid6;
		this.crosssellproductid7 = crosssellproductid7;
		this.crosssellproductid8 = crosssellproductid8;
		this.crosssellproductid9 = crosssellproductid9;
		this.crosssellproductid10 = crosssellproductid10;
		this.defaultparentcategoryid = defaultparentcategoryid;
		this.starttime = starttime;
		this.endtime = endtime;
		this.createdtime = createdtime;
		this.createdby = createdby;
		this.updatedtime = updatedtime;
		this.updatedby = updatedby;
		this.productmasterpropertieses = productmasterpropertieses;
		this.reviewsmasters = reviewsmasters;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "campaignid", nullable = false)
	public Campaign getCampaign() {
		return this.campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

	@Column(name = "productid", nullable = false, length = 50)
	public String getProductid() {
		return this.productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	@Column(name = "status", nullable = false)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "name", nullable = false, length = 250)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "iscollection", nullable = false)
	public int getIscollection() {
		return this.iscollection;
	}

	public void setIscollection(int iscollection) {
		this.iscollection = iscollection;
	}

	@Column(name = "islocked", nullable = false)
	public int getIslocked() {
		return this.islocked;
	}

	public void setIslocked(int islocked) {
		this.islocked = islocked;
	}

	@Column(name = "subproductid1", length = 50)
	public String getSubproductid1() {
		return this.subproductid1;
	}

	public void setSubproductid1(String subproductid1) {
		this.subproductid1 = subproductid1;
	}

	@Column(name = "subproductid2", length = 50)
	public String getSubproductid2() {
		return this.subproductid2;
	}

	public void setSubproductid2(String subproductid2) {
		this.subproductid2 = subproductid2;
	}

	@Column(name = "subproductid3", length = 50)
	public String getSubproductid3() {
		return this.subproductid3;
	}

	public void setSubproductid3(String subproductid3) {
		this.subproductid3 = subproductid3;
	}

	@Column(name = "subproductid4", length = 50)
	public String getSubproductid4() {
		return this.subproductid4;
	}

	public void setSubproductid4(String subproductid4) {
		this.subproductid4 = subproductid4;
	}

	@Column(name = "subproductid5", length = 50)
	public String getSubproductid5() {
		return this.subproductid5;
	}

	public void setSubproductid5(String subproductid5) {
		this.subproductid5 = subproductid5;
	}

	@Column(name = "subproductid6", length = 50)
	public String getSubproductid6() {
		return this.subproductid6;
	}

	public void setSubproductid6(String subproductid6) {
		this.subproductid6 = subproductid6;
	}

	@Column(name = "subproductid7", length = 50)
	public String getSubproductid7() {
		return this.subproductid7;
	}

	public void setSubproductid7(String subproductid7) {
		this.subproductid7 = subproductid7;
	}

	@Column(name = "subproductid8", length = 50)
	public String getSubproductid8() {
		return this.subproductid8;
	}

	public void setSubproductid8(String subproductid8) {
		this.subproductid8 = subproductid8;
	}

	@Column(name = "subproductid9", length = 50)
	public String getSubproductid9() {
		return this.subproductid9;
	}

	public void setSubproductid9(String subproductid9) {
		this.subproductid9 = subproductid9;
	}

	@Column(name = "subproductid10", length = 50)
	public String getSubproductid10() {
		return this.subproductid10;
	}

	public void setSubproductid10(String subproductid10) {
		this.subproductid10 = subproductid10;
	}

	@Column(name = "subproductid11", length = 50)
	public String getSubproductid11() {
		return this.subproductid11;
	}

	public void setSubproductid11(String subproductid11) {
		this.subproductid11 = subproductid11;
	}

	@Column(name = "subproductid12", length = 50)
	public String getSubproductid12() {
		return this.subproductid12;
	}

	public void setSubproductid12(String subproductid12) {
		this.subproductid12 = subproductid12;
	}

	@Column(name = "subproductid13", length = 50)
	public String getSubproductid13() {
		return this.subproductid13;
	}

	public void setSubproductid13(String subproductid13) {
		this.subproductid13 = subproductid13;
	}

	@Column(name = "subproductid14", length = 50)
	public String getSubproductid14() {
		return this.subproductid14;
	}

	public void setSubproductid14(String subproductid14) {
		this.subproductid14 = subproductid14;
	}

	@Column(name = "subproductid15", length = 50)
	public String getSubproductid15() {
		return this.subproductid15;
	}

	public void setSubproductid15(String subproductid15) {
		this.subproductid15 = subproductid15;
	}

	@Column(name = "upsellproductid1", length = 50)
	public String getUpsellproductid1() {
		return this.upsellproductid1;
	}

	public void setUpsellproductid1(String upsellproductid1) {
		this.upsellproductid1 = upsellproductid1;
	}

	@Column(name = "upsellproductid2", length = 50)
	public String getUpsellproductid2() {
		return this.upsellproductid2;
	}

	public void setUpsellproductid2(String upsellproductid2) {
		this.upsellproductid2 = upsellproductid2;
	}

	@Column(name = "upsellproductid3", length = 50)
	public String getUpsellproductid3() {
		return this.upsellproductid3;
	}

	public void setUpsellproductid3(String upsellproductid3) {
		this.upsellproductid3 = upsellproductid3;
	}

	@Column(name = "upsellproductid4", length = 50)
	public String getUpsellproductid4() {
		return this.upsellproductid4;
	}

	public void setUpsellproductid4(String upsellproductid4) {
		this.upsellproductid4 = upsellproductid4;
	}

	@Column(name = "upsellproductid5", length = 50)
	public String getUpsellproductid5() {
		return this.upsellproductid5;
	}

	public void setUpsellproductid5(String upsellproductid5) {
		this.upsellproductid5 = upsellproductid5;
	}

	@Column(name = "upsellproductid6", length = 50)
	public String getUpsellproductid6() {
		return this.upsellproductid6;
	}

	public void setUpsellproductid6(String upsellproductid6) {
		this.upsellproductid6 = upsellproductid6;
	}

	@Column(name = "upsellproductid7", length = 50)
	public String getUpsellproductid7() {
		return this.upsellproductid7;
	}

	public void setUpsellproductid7(String upsellproductid7) {
		this.upsellproductid7 = upsellproductid7;
	}

	@Column(name = "upsellproductid8", length = 50)
	public String getUpsellproductid8() {
		return this.upsellproductid8;
	}

	public void setUpsellproductid8(String upsellproductid8) {
		this.upsellproductid8 = upsellproductid8;
	}

	@Column(name = "upsellproductid9", length = 50)
	public String getUpsellproductid9() {
		return this.upsellproductid9;
	}

	public void setUpsellproductid9(String upsellproductid9) {
		this.upsellproductid9 = upsellproductid9;
	}

	@Column(name = "upsellproductid10", length = 50)
	public String getUpsellproductid10() {
		return this.upsellproductid10;
	}

	public void setUpsellproductid10(String upsellproductid10) {
		this.upsellproductid10 = upsellproductid10;
	}

	@Column(name = "crosssellproductid1", length = 50)
	public String getCrosssellproductid1() {
		return this.crosssellproductid1;
	}

	public void setCrosssellproductid1(String crosssellproductid1) {
		this.crosssellproductid1 = crosssellproductid1;
	}

	@Column(name = "crosssellproductid2", length = 50)
	public String getCrosssellproductid2() {
		return this.crosssellproductid2;
	}

	public void setCrosssellproductid2(String crosssellproductid2) {
		this.crosssellproductid2 = crosssellproductid2;
	}

	@Column(name = "crosssellproductid3", length = 50)
	public String getCrosssellproductid3() {
		return this.crosssellproductid3;
	}

	public void setCrosssellproductid3(String crosssellproductid3) {
		this.crosssellproductid3 = crosssellproductid3;
	}

	@Column(name = "crosssellproductid4", length = 50)
	public String getCrosssellproductid4() {
		return this.crosssellproductid4;
	}

	public void setCrosssellproductid4(String crosssellproductid4) {
		this.crosssellproductid4 = crosssellproductid4;
	}

	@Column(name = "crosssellproductid5", length = 50)
	public String getCrosssellproductid5() {
		return this.crosssellproductid5;
	}

	public void setCrosssellproductid5(String crosssellproductid5) {
		this.crosssellproductid5 = crosssellproductid5;
	}

	@Column(name = "crosssellproductid6", length = 50)
	public String getCrosssellproductid6() {
		return this.crosssellproductid6;
	}

	public void setCrosssellproductid6(String crosssellproductid6) {
		this.crosssellproductid6 = crosssellproductid6;
	}

	@Column(name = "crosssellproductid7", length = 50)
	public String getCrosssellproductid7() {
		return this.crosssellproductid7;
	}

	public void setCrosssellproductid7(String crosssellproductid7) {
		this.crosssellproductid7 = crosssellproductid7;
	}

	@Column(name = "crosssellproductid8", length = 50)
	public String getCrosssellproductid8() {
		return this.crosssellproductid8;
	}

	public void setCrosssellproductid8(String crosssellproductid8) {
		this.crosssellproductid8 = crosssellproductid8;
	}

	@Column(name = "crosssellproductid9", length = 50)
	public String getCrosssellproductid9() {
		return this.crosssellproductid9;
	}

	public void setCrosssellproductid9(String crosssellproductid9) {
		this.crosssellproductid9 = crosssellproductid9;
	}

	@Column(name = "crosssellproductid10", length = 50)
	public String getCrosssellproductid10() {
		return this.crosssellproductid10;
	}

	public void setCrosssellproductid10(String crosssellproductid10) {
		this.crosssellproductid10 = crosssellproductid10;
	}

	@Column(name = "defaultparentcategoryid", length = 50)
	public String getDefaultparentcategoryid() {
		return this.defaultparentcategoryid;
	}

	public void setDefaultparentcategoryid(String defaultparentcategoryid) {
		this.defaultparentcategoryid = defaultparentcategoryid;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "starttime", nullable = false, length = 19)
	public Date getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "endtime", nullable = false, length = 19)
	public Date getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "createdtime", nullable = false, length = 19)
	public Date getCreatedtime() {
		return this.createdtime;
	}

	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	@Column(name = "createdby", nullable = false)
	public long getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(long createdby) {
		this.createdby = createdby;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updatedtime", nullable = false, length = 19)
	public Date getUpdatedtime() {
		return this.updatedtime;
	}

	public void setUpdatedtime(Date updatedtime) {
		this.updatedtime = updatedtime;
	}

	@Column(name = "updatedby", nullable = false)
	public long getUpdatedby() {
		return this.updatedby;
	}

	public void setUpdatedby(long updatedby) {
		this.updatedby = updatedby;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productmaster")
	public Set<Productmasterproperties> getProductmasterpropertieses() {
		return this.productmasterpropertieses;
	}

	public void setProductmasterpropertieses(Set<Productmasterproperties> productmasterpropertieses) {
		this.productmasterpropertieses = productmasterpropertieses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "productmaster")
	public Set<Reviewsmaster> getReviewsmasters() {
		return this.reviewsmasters;
	}

	public void setReviewsmasters(Set<Reviewsmaster> reviewsmasters) {
		this.reviewsmasters = reviewsmasters;
	}

}
